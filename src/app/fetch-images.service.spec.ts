import { TestBed } from "@angular/core/testing";

import { FetchImagesService } from "./fetch-images.service";

describe("FetchImagesService", () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it("should be created", () => {
    const service: FetchImagesService = TestBed.get(FetchImagesService);
    expect(service).toBeTruthy();
  });

  it("should create array of images with numbername with given chunk size", (done: DoneFn) => {
    const service: FetchImagesService = TestBed.get(FetchImagesService);
    service.loadImages(0, 20);
    service.imagesSubject.asObservable().subscribe(val => {
      expect(val.offset).toBe(0);
      expect(val.images.length).toBe(20);
      done();
    });
  });
});
