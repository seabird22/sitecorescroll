import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { DeferLoadModule } from "@trademe/ng-defer-load";
import { VirtualScrollerModule } from "ngx-virtual-scroller";

import { AppComponent } from "./app.component";
import { FetchImagesService } from "./fetch-images.service";

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, VirtualScrollerModule],
  providers: [FetchImagesService],
  bootstrap: [AppComponent]
})
export class AppModule {}
