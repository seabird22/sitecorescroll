import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs/internal/BehaviorSubject";

@Injectable({
  providedIn: "root"
})
export class FetchImagesService {
  public imagesSubject: BehaviorSubject<
    imagesWithBatchNumber
  > = new BehaviorSubject<imagesWithBatchNumber>(null);

  constructor() {}

  public loadImages(offset: number, chunkSize) {
    let imagesList: { url: string; name?: string }[] = [];
    for (let i = 0; i < chunkSize; i++) {
      const url = "https://loremflickr.com/140/140?random=" + (offset + i + 1);
      imagesList[i] = {
        url: url,
        name: `Number ${offset + i + 1}`
      };
    }
    this.imagesSubject.next({ offset: offset, images: imagesList });
  }
}

interface imagesWithBatchNumber {
  images: { url: string; name?: string }[];
  offset: number;
}
