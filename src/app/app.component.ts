import { Component, ViewChild } from "@angular/core";
import { FetchImagesService } from "src/app/fetch-images.service";
import { VirtualScrollerComponent } from "ngx-virtual-scroller";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  title = "SiteCoreScroll";
  imagesList: any[] = [];
  chunkSize: number = 25;
  showMoreVisible: boolean = false;

  constructor(private fetchImagesService: FetchImagesService) {
    fetchImagesService.loadImages(0, 50);

    fetchImagesService.imagesSubject.asObservable().subscribe(val => {
      this.imagesList = this.imagesList.concat(val.images);
      console.log(this.imagesList);
    });
  }

  fetchMoreImages(event) {
    if (
      event.endIndex + 1 === this.imagesList.length &&
      event.endIndex + 1 < 200
    ) {
      console.log("Fetching new images", (event.endIndex + 1) / this.chunkSize);
      this.fetchImagesService.loadImages(event.endIndex + 1, this.chunkSize);
    } else {
      if (event.endIndex + 1 >= 200) this.showMoreVisible = true;
    }
  }

  showMore() {
    this.fetchImagesService.loadImages(this.imagesList.length, 200);
  }
}
