# SiteCoreScroll

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.0.3.

This slider is made with help of the ngx-virtual-scroller. This scroller keeps the number of items in the dom around constant so no overflow of the DOM is possible in the case of loading a large amount of pictures.
It works with infinite scroll with fetching 25 images dynamically when the user scrolls to the bottom. This chunksize can of course be modified.
After 200 images, infinite scroll is turned of and a button 'Show more' becomes visible to load instantaneously 200 pictures.
The loading of images is mocked by a service, which delivers a Subject (Observable) to listen for newly fetched images. This service is unit tested.
Furthermore is the image-list fully responsive and will reorder the number of images in one row when resizing the window.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
